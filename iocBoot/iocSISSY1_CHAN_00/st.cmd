#!../../bin/linux-x86_64/SISSY1_CHAN_00

#- You may have to change SISSY1_CHAN_00 to something else
#- everywhere it appears in this file

< envPaths

cd "${TOP}"

epicsEnvSet("SYS", "SISSY1EX")
epicsEnvSet("DEV", "Channeltron0")

epicsEnvSet("COM1", "RS232")
epicsEnvSet("STREAM_PROTOCOL_PATH","$(TOP)/db")
epicsEnvSet("ENGINEER","Will Smith")
epicsEnvSet("LOCATION", "$(SYS)")



epicsEnvSet("ALIVE_SERVER","sissy-serv-04.exp.helmholtz-berlin.de")

## Register all support components
dbLoadDatabase "dbd/SISSY1_CHAN_00.dbd"
SISSY1_CHAN_00_registerRecordDeviceDriver pdbbase

epicsEnvSet("MOXAIP", "172.17.9.13")
drvAsynIPPortConfigure("$(COM1)","$(MOXAIP):9001")

set_requestfile_path("$(TOP)/autosave", "")
set_savefile_path("/home/emil/Apps/autosave/$(SYS)/SISSY1_CHAN_00")
set_pass1_restoreFile("settings.sav")
set_pass0_restoreFile("settings.sav")
save_restoreSet_DatedBackupFiles(0)
## Load record instances

dbLoadRecords("$(TOP)/db/HighVoltage.db","P=$(SYS):$(DEV),PROTO=measar.proto,PORT=$(COM1) ")
dbLoadRecords("$(TOP)/db/DeadTime.db","P=$(SYS):$(DEV),PROTO=measar.proto,PORT=$(COM1) ")
dbLoadRecords("$(TOP)/db/Threshold.db","P=$(SYS):$(DEV),PROTO=measar.proto,PORT=$(COM1) ")
dbLoadRecords("$(TOP)/db/Interval.db","P=$(SYS):$(DEV),PROTO=measar.proto,PORT=$(COM1) ")
dbLoadRecords("$(TOP)/db/OVLimit.db","P=$(SYS):$(DEV),PROTO=measar.proto,PORT=$(COM1) ")
dbLoadRecords("$(TOP)/db/AutoTransmission.db","P=$(SYS):$(DEV),PROTO=measar.proto,PORT=$(COM1) ")
dbLoadRecords("$(TOP)/db/AnodeCurrent.db","P=$(SYS):$(DEV),PROTO=measar.proto,PORT=$(COM1) ")
dbLoadRecords("$(TOP)/db/Measurement.db","P=$(SYS):$(DEV),PROTO=measar.proto,PORT=$(COM1) ")
dbLoadRecords("$(ASYN)/db/asynRecord.db","P=$(SYS):,R=$(DEV):asyn,PORT=$(COM1),ADDR=-1,IMAX=0,OMAX=0")
dbLoadRecords("$(ALIVE)/db/alive.db", "P=$(SYS):$(DEV):,RHOST=$(ALIVE_SERVER)" )

cd "${TOP}/iocBoot/${IOC}"
iocInit

create_monitor_set("settings.req")
create_manual_set("settings.req")

#var streamDebug 1

dbpf $(SYS):$(DEV):Reset-CMD 1
dbpf $(SYS):$(DEV):Auto-SP 0